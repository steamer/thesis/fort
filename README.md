# FORT
The Foundational Ontological Relations Theory.
------------------------------------------------


## FORT-CL-ontology: 

* The ontology modules of FORT serialized in the CLIF dialect of the Common Logic standard. 
    
* The conversions to other dialects such as Prover9 and TPTP.
    
* The READMe file explains the followed procedure for the installation and usage of tha maceod tool.
    
    
## macleod: 

The macleod tool is available at the following link "https://github.com/thahmann/macleod". 
    
In this folder, we provide the *modified* configuration files of the local macleod repositoy in the home directory. These are:
    
* logging.conf
    
* macleod_win.conf
    
    
## VirtualEnvironment: 

The python virtual environment created for the installation of the macleod tool. 
    
In this folder, we provide the modified python parser files of the installed macleod tool in the virtual environment. These are:
    
* `Lib>site-packages>macleod>scripts>parser.py`
    
* `Lib>site-packages>macleod>parsing>parser.py`
    


More information about the theory will be available soon.
